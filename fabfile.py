# coding: utf-8
import os.path as op
import os
from fabric.api import run, env, cd, roles, get, prompt, sudo


env.use_ssh_config = True
env.roledefs['production'] = ['general_test_server']
project_name = "hatch"


def production_env():
    """Production enviroment"""
    env.key_filename = [op.join(os.environ['HOME'], '.ssh', 'id_rsa')]
    env.user = 'ubuntu'
    env.project_root = '/var/www/%s' % project_name
    # env.shell = '/usr/local/bin/bash -c'
    env.python = '/var/www/%s/env/bin/python' % project_name
    env.pip = '/var/www/%s/env/bin/pip' % project_name


@roles('production')
def deploy():
    production_env()
    with cd(env.project_root):
        run('git pull')
        run('sudo service uwsgi reload %s'% project_name)


@roles('production')
def pip_install():
    production_env()
    run('{pip} install --upgrade -r {filepath}'.format(
        pip=env.pip,
        filepath=op.join(env.project_root, 'requirements.txt')
        )
        )


@roles('production')
def get_backup():
    production_env()
    backup_name = "current_%s.backup" % project_name
    run('pg_dump -w -U postgres -F c -f ~/{0} {1}'.format(backup_name, project_name))
    get('~/{0}'.format(backup_name), '~/backups/')


@roles('production')
def logs():
    production_env()
    run('sudo cat /var/log/uwsgi/app/{0}.log'.format(project_name))


@roles('production')
def psql(command):
    production_env()
    run('psql -U techuser {0} -c "{1}"'.format(project_name, command))