alter table events rename column price to price_real;
alter table events add column price_discount numeric;
create table events_old as select * from events;
drop table events;
insert into categories(name) values('music');
INSERT INTO events(
           name, tstamp, img, place, city, price_real, descr, price_discount,
           category_id)
select name, tstamp, img, place, city, price_real, descr, price_real-10, (select id from categories where name = 'music' limit 1)
from events_old;
drop table events_old;