#!/usr/bin/env python
# -*- coding: utf-8 -*-

from utils import print_obj
from flask import Flask, render_template
# from flask.ext.restless import APIManager, ProcessingException
from extensions import db, admin, login, mail
from views import common
from api import api
from util_views import dev


app = Flask(__name__, template_folder='../templates',
            static_folder='../static', static_url_path ='')
app.config.from_object('config')

#login init
login.init_app(app)

# init db
db.app = app
db.init_app(app)

# init administration page
admin.init_app(app)
from admin import admin

#init mail
mail.init_app(app)

# register blueprint
app.register_blueprint(common)
app.register_blueprint(api, url_prefix='/api')
app.register_blueprint(dev, url_prefix='/dev')
