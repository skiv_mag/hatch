
import os
import os.path as op
import shutil
from pygeocoder import Geocoder, GeocoderError

from extensions import admin, db
from models import Events, Categories, Payments
import config as CFG
from utils import print_obj

from flask.ext.admin.contrib.fileadmin import FileAdmin
from flask.ext.admin.contrib.sqla import ModelView
from flask.ext.admin import form
import flask.ext.login as login
from wtforms import validators
from flask import url_for, Markup


class AuthMixin(object):

    def is_accessible(self):
        return login.current_user.is_authenticated()


class AuthAdmin(AuthMixin, ModelView):
    pass


class PaymentAdmin(AuthAdmin):

    column_display_pk = True
    can_create = False
    can_edit = False
    can_delete = False


class EventsAdmin(ModelView):

    column_exclude_list = ('lat', 'lng')
    form_excluded_columns = ('lat', 'lng', 'bought_tickets')
    edit_template = 'events_edit.html'
    create_template = 'events_create.html'


    def _long_text(view, context, model, name):
        descr = model.descr
        if len(model.descr) > 60:
            descr = model.descr.split(".")[0].split("!")[0] + "..."
        return descr

    def on_model_change(self, form, model, is_created):
        # calculate long ant lat by adress
        try:
            adress = form.data["city"] + "," + form.data["place"]
            results = Geocoder.geocode(adress)
            lat, lng = results[0].coordinates
            model.update(lat=lat, lng=lng)
        except GeocoderError, e:
            print e
            pass

    def after_model_change(self, form, model, is_created):
        img_dir = op.join(CFG.IMG_ROOT, model.img_folder)
        if not is_created:
            prev_dir = None
            for d in os.listdir(path):
                # A little bit hack to find previous folder by id
                if d.startswith("event_%s_" % model.id):
                    prev_dir = d
                    break
            if prev_dir:
                prev_root = op.join(CFG.IMG_ROOT, prev_dir)
                os.rename(prev_root, img_dir)
        if not op.exists(img_dir):
            os.makedirs(img_dir)

    def on_model_delete(self, model):
        img_dir = op.join(CFG.IMG_ROOT, model.img_folder)
        shutil.rmtree(img_dir)

    column_formatters = {
        "descr": _long_text
    }

    form_args = dict(
        city=dict(validators=[validators.Required()]),
        place = dict(validators=[validators.Required()]),
        org_email = dict(label='Organizer Email', validators=[validators.Required(), validators.Email()]),
        price_discount = dict(label='Price with discount', validators=[validators.Required()]),
        name = dict(validators=[validators.Required()]),
        category = dict(validators=[validators.Required()]),
        price_real = dict(validators=[validators.Required()]),
        availible_tickets = dict(validators=[validators.Required()])
    )


class ImagesAdmin(AuthMixin, FileAdmin):
    
    allowed_extensions = ('jpg', 'jpeg', 'png')

    upload_template = "images_upload.html"


class ExportAdmin(AuthMixin, FileAdmin):
    
    list_template = 'images_list.html'
    can_mkdir = False
    can_upload = False


admin.add_view(EventsAdmin(Events, db.session))
admin.add_view(AuthAdmin(Categories, db.session))
admin.add_view(PaymentAdmin(Payments, db.session))

admin.add_view(ImagesAdmin(CFG.IMG_ROOT, '/img/', name='Images'))
admin.add_view(ExportAdmin(CFG.CSV_ROOT, '/csv/', name='CSV'))
