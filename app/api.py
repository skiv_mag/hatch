#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from flask.views import MethodView
from flask import (Blueprint, request, current_app)

from utils import jsonify, as_dict_many, print_obj, filter_params
from models import Events, Payments
from extensions import stripe

api = Blueprint('api', 'api')


@api.before_request
def api_br():
    if request.method == 'GET' and request.args:
        current_app.logger.info(request.args)
    if request.method == 'POST' and request.form:
        current_app.logger.info(request.form)


class EventsAPI(MethodView):

    def get(self):
        allowed_params = ["date_from", "date_to", "category", "limit"]
        ret_args = filter_params(request.args, allowed_params)

        cond = []

        if "date_from" in ret_args:
            cond.append(Events.tstamp >= ret_args["date_from"])
        if "date_to" in ret_args:
            cond.append(Events.tstamp <= ret_args["date_to"])
        if "category" in ret_args:
            cond.append(Events.category.has(name=ret_args["category"]))
        if "limit" in ret_args:
            try:
                limit = int(ret_args["limit"])
            except:
                return jsonify({"error": "Limit must be integer"})

        events = Events.query.filter(*cond)
        if "limit" in ret_args:
            events = events.limit(limit)
        else:
            events = events.all()

        return jsonify(as_dict_many(events, exclude=['category_id']))


@api.route('/charge', methods=['POST'])
def charge():

    allowed_params = [
        "email", "quantity", "stripeToken", "name", "event_id", "hotel"]
    ret_args = filter_params(request.form, allowed_params)
    event = Events.get(ret_args["event_id"])

    try:
        ret_args['quantity'] = int(ret_args['quantity'])
    except ValueError:
        return jsonify({"error": "not valid quantity"})

    if not event:
        return jsonify({"error": "Event not exists"})

    payment = {}
    try:
        amount = ret_args['quantity'] * event.price_discount
        payment.update({
            "email": ret_args['email'],
            "amount": amount,
            "name": ret_args["name"],
            "hotel_name": ret_args["hotel"]
        })
        customer = stripe.Customer.create(
            email=ret_args['email'],
            card=ret_args['stripeToken']
        )

        customer_id = customer.id
        payment.update({
            "customer_id": customer.id
            }
        )

        charge = stripe.Charge.create(
            customer=customer_id,
            # stripe sends data to server in cents
            amount=int(amount*100),
            currency='eur',
            description=ret_args["name"]
        )
        payment.update({
            "charge_id": charge.id,
            "status": True
            }
        )

        event.update(
            availible_tickets=event.availible_tickets - 1,
            bought_tickets=event.bought_tickets + 1
        )

        
    except stripe.error.CardError, e:
        payment.update({
            "error": json.dumps(e.json_body),
            "status": False
            }
        )
        print payment
        Payments.create(**payment)
        return jsonify(e.json_body)
    except:
        current_app.logger.error("Unexpected error")
        raise
    else:
        Payments.create(**payment)
        return jsonify({"charge_id": charge.id})


api.add_url_rule('/events', view_func=EventsAPI.as_view('users'))
