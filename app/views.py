#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path as op
import csv

from utils import print_obj, as_dict_many
from extensions import mail
import config as CFG
from models import Payments

from flask import Blueprint, request, render_template, Response, redirect
from flask.ext.mail import Message


common = Blueprint('common_page', 'common_page')


@common.route('/')
def api():
    return "Blueprint works"


@common.route('/about')
def about():
    return render_template("about.html")


@common.route('/send_email', methods=["POST"])
def send_email():
    email_form = request.form
    if email_form:
        msg = Message(
            "Hello",
            sender="Contact Usher",
            recipients=CFG.ADMIN_EMAIL,
            body=email_form["email_text"]
        )
    mail.send(msg)
    return 'ok'


@common.route('/send_ticket', methods=["POST"])
def send_ticket():
    email_form = request.form
    print_obj(request)
    if email_form:
        mails = CFG.ADMIN_EMAIL + list(email_form["email_text"])
        msg = Message(
            "Hello",
            sender="Contact Usher",
            recipients=mails,
            body=email_form["email_text"]
        )
    mail.send(msg)
    return 'ok'


@common.route('/export_payments')
def export_payments():
    payments = as_dict_many(Payments.query.all())
    with open(op.join(CFG.CSV_ROOT, 'payments.csv'), 'wb') as f:
        w = csv.DictWriter(f, payments[0].keys())
        w.writeheader()
        [w.writerow(row) for row in payments]
    return redirect('/admin/exportadmin/')
