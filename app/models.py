#!/usr/bin/env python
# -*- coding: utf-8 -*-

from extensions import db
from mixin import CRUDMixin
import config as CFG

from datetime import datetime
import os
import os.path as op

from werkzeug.security import (
    generate_password_hash,
    check_password_hash
)
# from sqlalchemy.event import listens_for
from sqlalchemy.ext.hybrid import hybrid_property
from flask import url_for


class Users(db.Model, CRUDMixin):
    login = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))

    def __init__(self, login, password):
        self.login = login
        self.set_password(password)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.login


class Categories(db.Model, CRUDMixin):

    name = db.Column(db.String(256))

    def __unicode__(self):
        return self.name


class Events(db.Model, CRUDMixin):
    name = db.Column(db.String(256))
    tstamp = db.Column(db.DateTime,
                       default=datetime.utcnow().replace(second=0, microsecond=0))
    place = db.Column(db.String(256))
    city = db.Column(db.String(256))
    price_real = db.Column(db.Numeric(12, 2))
    price_discount = db.Column(db.Numeric(12, 2))
    descr = db.Column(db.UnicodeText)
    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
    lat = db.Column(db.Numeric(12, 6))
    lng = db.Column(db.Numeric(12, 6))
    org_email = db.Column(db.String(256))
    availible_tickets = db.Column(db.Integer)
    bought_tickets = db.Column(db.Integer, default=0)

    category = db.relationship('Categories')

    def __unicode__(self):
        return self.name

    def get_img_url(self, img_name):
        return url_for(
            'static',
            filename=op.join('img', self.img_folder, img_name)
        )

    @property
    def img_folder(self):
        return "event_{0}_{1}".format(self.id, self.name)

    @hybrid_property
    def images(self):
        abs_folder = op.join(CFG.IMG_ROOT, self.img_folder)
        return sorted([self.get_img_url(f) for f in os.listdir(abs_folder)])

    @hybrid_property
    def img_url(self):
        return self.images[0] if self.images else None


class Payments(db.Model, CRUDMixin):

    hotel_name = db.Column(db.String(256))
    email = db.Column(db.String(256))
    amount = db.Column(db.Numeric(12, 2))
    name = db.Column(db.String(256))
    customer_id = db.Column(db.String(256))
    charge_id = db.Column(db.String(256))
    event_id = db.Column(db.Integer, db.ForeignKey('events.id'))
    status = db.Column(db.Boolean())
    error = db.Column(db.String(256))
    tstamp = db.Column(db.DateTime, default=datetime.utcnow())

    def __unicode__(self):
        return self.name
