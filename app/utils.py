#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pprint import pprint
import time
import datetime
import decimal
import json

from flask import Response


def print_obj(object_name):
    try:
        pprint({x: getattr(object_name, x) for x in dir(object_name)})
    except:
        for x in dir(object_name):
            try:
                print {x: getattr(object_name, x)}
            except:
                'cant print %s' % x


def json_handler(obj):
    if isinstance(obj, datetime.date) or isinstance(obj, datetime.datetime):
        return obj.isoformat()
    elif isinstance(obj, decimal.Decimal):
        return str(obj)
    else:
        return None


def jsonify(data, status=200):
    """
    Custom jsonify function, supports lists
    """
    timestamp = time.time()
    last_mod = time.strftime(
        '%a, %d %b %Y %H:%M:%S GMT', time.gmtime(timestamp))
    return Response(
        response=json.dumps(data, default=json_handler),
        content_type='application/json',
        status=status,
        headers={
            'If-Modified-Since': last_mod,
            'Cache-Control': True
        }
    )


def as_dict_many(obj_list, **kwargs):
    return map(lambda x: x.as_dict(**kwargs), obj_list)


def as_dict_many_ext(query_obj):
    """Use when session.query() query"""
    return [dict(zip(obj.keys(), obj)) for obj in query_obj]


def filter_params(args, filter_list):
    if args:
        return {k: v for k, v in args.iteritems() if k in filter_list}
    else:
        return {}
