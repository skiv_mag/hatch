#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.admin import Admin
from flask.ext.login import LoginManager
from flask.ext.mail import Mail
import stripe

db = SQLAlchemy()
login = LoginManager()
mail=Mail()

from auth import MyAdminIndexView

admin = Admin(name="Usher Admin",
              index_view=MyAdminIndexView(), base_template='my_master.html')

stripe_keys = {
    'secret_key': 'sk_test_UBuKWLhwVEoSqfxY00uj1T3J',
    'publishable_key': 'pk_test_KcqQaYPubZiGI1NatF3KaiNQ'
}

stripe.api_key = stripe_keys['secret_key']
