#!/usr/bin/env python
# -*- coding: utf-8 -*-


from extensions import db
from flask import Blueprint

dev = Blueprint('developer_instruments', 'dev')


@dev.route('/clean_all', methods=['GET'])
def clear():
    for table in reversed(db.metadata.sorted_tables):
        table.delete()
    db.session.commit()
    return "ok"


@dev.route('/drop_all', methods=['GET'])
def drop():
    db.drop_all()
    return "ok"


@dev.route('/create_all', methods=['GET'])
def create():
    db.create_all()
    return "ok"


@dev.route('/recreate_all', methods=['GET'])
def recreate():
    db.drop_all()
    db.create_all()
    return "ok"


@dev.route('/clean/<table_name>', methods=['GET'])
def clear_one(table_name):
    try:
        table_name.delete()
        return "ok"
    except:
        return "fail"


@dev.route('/drop/<table_name>', methods=['GET'])
def drop_one():
    return "ok"


@dev.route('/create/<table_name>', methods=['GET'])
def create_one():
    return "ok"
